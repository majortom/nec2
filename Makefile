RM= rm -f
F2C= ~/f2c
F77= f77
SRCS=nec2.f nec2small.f secnds.f
CSRCS=nec2.c nec2small.c secnds.c
OBJS=nec2.o secnds.o
OBJSSMALL=nec2small.o secnds.o
FFLAGS= -O2

all: nec2 nec2small

nec2: $(OBJS)
	$(F2C) $(SRCS)
	gcc -c $(CSRCS)
	gcc -o nec2 $(OBJS) -lf2c -lm

nec2small: $(OBJSSMALL)
	gcc -o nec2small $(OBJSSMALL) -lf2c -lm

clean:
	$(RM) *.o
	$(RM) nec2
	$(RM) nec2small
