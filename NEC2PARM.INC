C Change all these params
C .. N is number segments
C .. M is number patches
C .. NM = N + M, N2M = N+2*M, N3M = N+3*M
C .. So here the default is configured for 5000 segments and patches
      PARAMETER ( NM=10000, N2M=15000, N3M=20000)
